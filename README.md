## KOALA - KEGG Offline Archive Logic Analysis ##

KOALA evaluates KEGG module definitions and tests if conditions are fulfilled based on a given list of genes



### Setup: ###

You need to have a file which contains the module definitions as they appear on the KEGG Website.

The archive file must contain definitions of the modules you want to test your genes against. For every module entry there must be three lines in the following order and format:

```
ENTRY         {ModuleName ... ... ...}
NAME          {ModuleDescription}
DEFINITION    {Definition}
```

Everything else will be ignored

### Usage: ###

```
#!bash

./koala --archive /path/to/your/archive.file gene1 gene2 gene3 ..
```

### Example: ###

```
#!bash

./koala --archive /path/to/your/archive.file M00567 
M00617
```


```
#!bash

koala --archive /path/to/your/archive.file --verbose M00567 
M00617  -  Methanogen  -  http://genome.jp/kegg-bin/show_module?M00617
M00567,M00357,M00356,M00563
```